﻿namespace WindowsFormsApp1
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbResult = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.tbFirstOperand = new System.Windows.Forms.TextBox();
            this.tbSecondOperand = new System.Windows.Forms.TextBox();
            this.bDiv = new System.Windows.Forms.Button();
            this.bMulti = new System.Windows.Forms.Button();
            this.bMinus = new System.Windows.Forms.Button();
            this.bPlus = new System.Windows.Forms.Button();
            this.bClear = new System.Windows.Forms.Button();
            this.bSqrt = new System.Windows.Forms.Button();
            this.bSin = new System.Windows.Forms.Button();
            this.bCos = new System.Windows.Forms.Button();
            this.bLog = new System.Windows.Forms.Button();
            this.bPow = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lbResult
            // 
            this.lbResult.AutoSize = true;
            this.lbResult.Location = new System.Drawing.Point(452, 82);
            this.lbResult.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lbResult.Name = "lbResult";
            this.lbResult.Size = new System.Drawing.Size(22, 13);
            this.lbResult.TabIndex = 0;
            this.lbResult.Text = "0.0";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(87, 58);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(70, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "First Operand";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(245, 58);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(88, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Second Operand";
            // 
            // tbFirstOperand
            // 
            this.tbFirstOperand.Location = new System.Drawing.Point(89, 82);
            this.tbFirstOperand.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.tbFirstOperand.Name = "tbFirstOperand";
            this.tbFirstOperand.Size = new System.Drawing.Size(76, 20);
            this.tbFirstOperand.TabIndex = 3;
            // 
            // tbSecondOperand
            // 
            this.tbSecondOperand.Location = new System.Drawing.Point(248, 82);
            this.tbSecondOperand.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.tbSecondOperand.Name = "tbSecondOperand";
            this.tbSecondOperand.Size = new System.Drawing.Size(76, 20);
            this.tbSecondOperand.TabIndex = 4;
            this.tbSecondOperand.TextChanged += new System.EventHandler(this.tbSecondOperand_TextChanged);
            // 
            // bDiv
            // 
            this.bDiv.Location = new System.Drawing.Point(89, 197);
            this.bDiv.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.bDiv.Name = "bDiv";
            this.bDiv.Size = new System.Drawing.Size(56, 19);
            this.bDiv.TabIndex = 5;
            this.bDiv.Text = "/";
            this.bDiv.UseVisualStyleBackColor = true;
            this.bDiv.Click += new System.EventHandler(this.bDiv_Click);
            // 
            // bMulti
            // 
            this.bMulti.Location = new System.Drawing.Point(187, 197);
            this.bMulti.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.bMulti.Name = "bMulti";
            this.bMulti.Size = new System.Drawing.Size(56, 19);
            this.bMulti.TabIndex = 6;
            this.bMulti.Text = "*";
            this.bMulti.UseVisualStyleBackColor = true;
            this.bMulti.Click += new System.EventHandler(this.bMulti_Click);
            // 
            // bMinus
            // 
            this.bMinus.Location = new System.Drawing.Point(286, 198);
            this.bMinus.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.bMinus.Name = "bMinus";
            this.bMinus.Size = new System.Drawing.Size(56, 19);
            this.bMinus.TabIndex = 7;
            this.bMinus.Text = "-";
            this.bMinus.UseVisualStyleBackColor = true;
            this.bMinus.Click += new System.EventHandler(this.bMinus_Click);
            // 
            // bPlus
            // 
            this.bPlus.Location = new System.Drawing.Point(378, 197);
            this.bPlus.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.bPlus.Name = "bPlus";
            this.bPlus.Size = new System.Drawing.Size(56, 19);
            this.bPlus.TabIndex = 8;
            this.bPlus.Text = "+";
            this.bPlus.UseVisualStyleBackColor = true;
            this.bPlus.Click += new System.EventHandler(this.bPlus_Click);
            // 
            // bClear
            // 
            this.bClear.Location = new System.Drawing.Point(466, 197);
            this.bClear.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.bClear.Name = "bClear";
            this.bClear.Size = new System.Drawing.Size(56, 19);
            this.bClear.TabIndex = 9;
            this.bClear.Text = "CE";
            this.bClear.UseVisualStyleBackColor = true;
            this.bClear.Click += new System.EventHandler(this.bClear_Click);
            // 
            // bSqrt
            // 
            this.bSqrt.Location = new System.Drawing.Point(89, 252);
            this.bSqrt.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.bSqrt.Name = "bSqrt";
            this.bSqrt.Size = new System.Drawing.Size(56, 19);
            this.bSqrt.TabIndex = 10;
            this.bSqrt.Text = "Sqrt";
            this.bSqrt.UseVisualStyleBackColor = true;
            this.bSqrt.Click += new System.EventHandler(this.bSqrt_Click);
            // 
            // bSin
            // 
            this.bSin.Location = new System.Drawing.Point(187, 252);
            this.bSin.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.bSin.Name = "bSin";
            this.bSin.Size = new System.Drawing.Size(56, 19);
            this.bSin.TabIndex = 11;
            this.bSin.Text = "Sin";
            this.bSin.UseVisualStyleBackColor = true;
            this.bSin.Click += new System.EventHandler(this.bSin_Click);
            // 
            // bCos
            // 
            this.bCos.Location = new System.Drawing.Point(286, 252);
            this.bCos.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.bCos.Name = "bCos";
            this.bCos.Size = new System.Drawing.Size(56, 19);
            this.bCos.TabIndex = 12;
            this.bCos.Text = "Cos";
            this.bCos.UseVisualStyleBackColor = true;
            this.bCos.Click += new System.EventHandler(this.bCos_Click);
            // 
            // bLog
            // 
            this.bLog.Location = new System.Drawing.Point(378, 252);
            this.bLog.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.bLog.Name = "bLog";
            this.bLog.Size = new System.Drawing.Size(56, 19);
            this.bLog.TabIndex = 13;
            this.bLog.Text = "Log";
            this.bLog.UseVisualStyleBackColor = true;
            this.bLog.Click += new System.EventHandler(this.bLog_Click);
            // 
            // bPow
            // 
            this.bPow.Location = new System.Drawing.Point(466, 252);
            this.bPow.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.bPow.Name = "bPow";
            this.bPow.Size = new System.Drawing.Size(56, 19);
            this.bPow.TabIndex = 14;
            this.bPow.Text = "Pow";
            this.bPow.UseVisualStyleBackColor = true;
            this.bPow.Click += new System.EventHandler(this.bPow_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(600, 366);
            this.Controls.Add(this.bPow);
            this.Controls.Add(this.bLog);
            this.Controls.Add(this.bCos);
            this.Controls.Add(this.bSin);
            this.Controls.Add(this.bSqrt);
            this.Controls.Add(this.bClear);
            this.Controls.Add(this.bPlus);
            this.Controls.Add(this.bMinus);
            this.Controls.Add(this.bMulti);
            this.Controls.Add(this.bDiv);
            this.Controls.Add(this.tbSecondOperand);
            this.Controls.Add(this.tbFirstOperand);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.lbResult);
            this.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lbResult;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox tbFirstOperand;
        private System.Windows.Forms.TextBox tbSecondOperand;
        private System.Windows.Forms.Button bDiv;
        private System.Windows.Forms.Button bMulti;
        private System.Windows.Forms.Button bMinus;
        private System.Windows.Forms.Button bPlus;
        private System.Windows.Forms.Button bClear;
        private System.Windows.Forms.Button bSqrt;
        private System.Windows.Forms.Button bSin;
        private System.Windows.Forms.Button bCos;
        private System.Windows.Forms.Button bLog;
        private System.Windows.Forms.Button bPow;
    }
}

