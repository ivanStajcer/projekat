﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void bDiv_Click(object sender, EventArgs e)
        {
            try
            {
                double a = double.Parse(tbFirstOperand.Text);
                double mB = double.Parse(tbSecondOperand.Text);
                double result = a / mB;
                lbResult.Text = result.ToString();
            }
            catch (Exception)
            {
                MessageBox.Show("Greška");
            }
            
        }

        private void bMulti_Click(object sender, EventArgs e)
        {
            try
            {
                double a = double.Parse(tbFirstOperand.Text);
                double mB = double.Parse(tbSecondOperand.Text);
                double result = a * mB;
                lbResult.Text = result.ToString();
            }
            catch (Exception)
            {
                MessageBox.Show("Greška");
            }
        }

        private void bMinus_Click(object sender, EventArgs e)
        {
            try
            {
                double a = double.Parse(tbFirstOperand.Text);
                double mB = double.Parse(tbSecondOperand.Text);
                double result = a - mB;
                lbResult.Text = result.ToString();
            }
            catch (Exception)
            {
                MessageBox.Show("Greška");
            }
        }

        private void bPlus_Click(object sender, EventArgs e)
        {
            try
            {
                double a = double.Parse(tbFirstOperand.Text);
                double mB = double.Parse(tbSecondOperand.Text);
                double result = a + mB;
                lbResult.Text = result.ToString();
            }
            catch (Exception)
            {
                MessageBox.Show("Greška");
            }
        }

        private void bClear_Click(object sender, EventArgs e)
        {
            lbResult.Text = "0.0";
        }

        private void bSqrt_Click(object sender, EventArgs e)
        {
            try
            {
                double a = double.Parse(tbFirstOperand.Text);
                double mB = double.Parse(tbSecondOperand.Text);
                double result = Math.Sqrt(a);
                lbResult.Text = result.ToString();
            }
            catch (Exception)
            {
                MessageBox.Show("Greška");
            }
        }

        private void bSin_Click(object sender, EventArgs e)
        {
            try
            {
                double a = double.Parse(tbFirstOperand.Text);
                double mB = double.Parse(tbSecondOperand.Text);
                double result = Math.Sin(a);
                lbResult.Text = result.ToString();
            }
            catch (Exception)
            {
                MessageBox.Show("Greška");
            }
        }

        private void bCos_Click(object sender, EventArgs e)
        {
            try
            {
                double a = double.Parse(tbFirstOperand.Text);
                double mB = double.Parse(tbSecondOperand.Text);
                double result = Math.Cos(mB);
                lbResult.Text = result.ToString();
            }
            catch (Exception)
            {
                MessageBox.Show("Greška");
            }
        }

        private void bLog_Click(object sender, EventArgs e)
        {
            try
            {
                double a = double.Parse(tbFirstOperand.Text);
                double mB = double.Parse(tbSecondOperand.Text);
                double result = Math.Log(a);
                lbResult.Text = result.ToString();
            }
            catch (Exception)
            {
                MessageBox.Show("Invalid input");
            }
        }

        private void bPow_Click(object sender, EventArgs e)
        {
            try
            {
                double a = double.Parse(tbFirstOperand.Text);
                double mB = double.Parse(tbSecondOperand.Text);
                double result = Math.Pow(a, mB);
                lbResult.Text = result.ToString();
            }
            catch (Exception)
            {
                MessageBox.Show("Greška");
            }
        }

        private void tbSecondOperand_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
